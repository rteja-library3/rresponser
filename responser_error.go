package rresponser

import (
	"fmt"
	"net/http"

	apperror "gitlab.com/rteja-library3/rapperror"
	helper "gitlab.com/rteja-library3/rhelper"
)

type responserError struct {
	code   string
	msg    string
	status int
	err    error
	data   interface{}
}

func NewResponserErrorWithMessage(err error, message, code string) Responser {
	// make sure  error is not nil, its not possible seharusny
	if err == nil {
		return &responserError{
			code:   code,
			msg:    message,
			status: http.StatusInternalServerError,
			err:    fmt.Errorf("something wrong"),
		}
	}

	// default is 500 / unkown
	var cde string = helper.StringOr(code, apperror.AppErrorCodeInternalServerError.String())
	var msg string = helper.StringOr(message, err.Error())
	var status int = http.StatusInternalServerError
	var data interface{} = nil

	// set app error
	switch v := err.(type) {
	case *apperror.AppError:
		cde = helper.StringOr(code, v.Code.String())
		msg = helper.StringOr(message, v.Message)
		status = v.Status
		data = v.Data
	}

	return &responserError{
		code:   cde,
		msg:    msg,
		status: status,
		err:    err,
		data:   data,
	}
}

func NewResponserError(err error) Responser {
	return NewResponserErrorWithMessage(err, "", "")
}

func (r responserError) HTTPStatus() int {
	return r.status
}

func (r responserError) Code() string {
	return r.code
}

func (r responserError) Message() string {
	return r.msg
}

func (r responserError) Data() interface{} {
	return r.data
}

func (r responserError) Meta() interface{} {
	return nil
}

func (r responserError) Error() error {
	return r.err
}
