package rresponser

import "net/http"

func NewResponserSuccessCreated(code, message string, data, meta interface{}) Responser {
	return newResponserSuccess(http.StatusCreated, code, message, data, meta)
}

func NewResponserSuccessOK(code, message string, data, meta interface{}) Responser {
	return newResponserSuccess(http.StatusOK, code, message, data, meta)
}
