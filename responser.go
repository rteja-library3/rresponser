package rresponser

type Responser interface {
	HTTPStatus() int
	Data() interface{}
	Error() error
	Message() string
	Code() string
	Meta() interface{}
}
