package rresponser

type responserSuccess struct {
	code   string
	msg    string
	status int
	data   interface{}
	meta   interface{}
}

func newResponserSuccess(status int, code, message string, data, meta interface{}) Responser {
	if code == "" {
		code = "success"
	}

	return &responserSuccess{
		status: status,
		code:   code,
		msg:    message,
		data:   data,
		meta:   meta,
	}
}

func (r responserSuccess) HTTPStatus() int {
	return r.status
}

func (r responserSuccess) Code() string {
	return r.code
}

func (r responserSuccess) Message() string {
	return r.msg
}

func (r responserSuccess) Data() interface{} {
	return r.data
}

func (r responserSuccess) Meta() interface{} {
	return r.meta
}

func (r responserSuccess) Error() error {
	return nil
}
