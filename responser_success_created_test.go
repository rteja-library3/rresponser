package rresponser_test

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/rteja-library3/rresponser"
)

func TestNewResponserSuccessCreated(t *testing.T) {
	data := "data-test"
	meta := "meta-test"

	resp := rresponser.NewResponserSuccessCreated("", "message-should here", data, meta)

	assert.NoError(t, resp.Error(), "[TestNewResponserSuccessCreated] Should not error")
	assert.Equal(t, "data-test", resp.Data(), "[TestNewResponserSuccessCreated] Data should \"data-test\"")
	assert.Equal(t, "meta-test", resp.Meta(), "[TestNewResponserSuccessCreated] Meta should \"meta-test\"")
	assert.Equal(t, "message-should here", resp.Message(), "[TestNewResponserSuccessCreated] Message should \"message-should here\"")
	assert.Equal(t, http.StatusCreated, resp.HTTPStatus(), "[TestNewResponserSuccessCreated] HTTP Status should 201")
	assert.Equal(t, "success", resp.Code(), "[TestNewResponserSuccessCreated] Code should \"success\"")
}

func TestNewResponserSuccessOK(t *testing.T) {
	data := "data-test"
	meta := "meta-test"

	resp := rresponser.NewResponserSuccessOK("", "message-should here", data, meta)

	assert.NoError(t, resp.Error(), "[TestNewResponserSuccessOK] Should not error")
	assert.Equal(t, "data-test", resp.Data(), "[TestNewResponserSuccessOK] Data should \"data-test\"")
	assert.Equal(t, "meta-test", resp.Meta(), "[TestNewResponserSuccessOK] Meta should \"meta-test\"")
	assert.Equal(t, "message-should here", resp.Message(), "[TestNewResponserSuccessOK] Message should \"message-should here\"")
	assert.Equal(t, http.StatusOK, resp.HTTPStatus(), "[TestNewResponserSuccessOK] HTTP Status should 200")
	assert.Equal(t, "success", resp.Code(), "[TestNewResponserSuccessOK] Code should \"success\"")
}
